package ru.mail.course;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

public enum LoggerType {
    CONSOLE("c"),
    FILE("f"),
    COMPOSITE("cf");

    @NotNull
    private String flag;

    @NotNull
    private static Map<String, LoggerType> valueByFlag = new HashMap<String, LoggerType>(){{
        put("c", CONSOLE);
        put("f", FILE);
        put("cf", COMPOSITE);
    }};

    LoggerType(@NotNull String flag) {
        this.flag = flag;
    }

    @Nullable
    public static LoggerType get(@NotNull String flag) {
        return valueByFlag.get(flag);
    }
}
