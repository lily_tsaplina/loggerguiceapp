package ru.mail.course;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.annotation.Logging;

import java.util.NoSuchElementException;
import java.util.Scanner;

@SuppressWarnings("FinalOrAbstractClass")
public class Application {

    /**
     * Entry point.
     * @param args expected args:
     *             logger type as "c" (console), "f" (file) or "cf" (console+file),
     *             file name if "f" or "cf",
     *             tag if "f" or "cf".
     *             Example: cf log.txt p.
     */
    public static void main(@NotNull String[] args) {
        String loggerType = "c";
        String fileName = "";
        String tag = "";
        if (args.length > 0) {
            loggerType = args[0];
            if (loggerType.contains("f")) {
                if (args.length > 1) {
                    fileName = args[1];
                    if (args.length == 3) {
                        tag = args[2];
                    }
                }
            }
        }

        LoggerGuiceModule module = new LoggerGuiceModule(LoggerType.get(loggerType), fileName, tag);
        final Injector injector = Guice.createInjector(module);
        injector.getInstance(Application.class).waitForInput();
    }   

    private void waitForInput() {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Waiting for new lines. Key in Ctrl+D to exit.");
            //noinspection InfiniteLoopStatement
            while (true) {
                String inputLine = scanner.nextLine();
                processInputLine(inputLine);
            }
        } catch (IllegalStateException | NoSuchElementException ignored) {
        }
    }

    @Logging
    void processInputLine(@NotNull String input) {}
}
