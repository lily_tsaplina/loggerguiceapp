package ru.mail.course.interceptor;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class ConsoleCountingInterceptor extends CountingInterceptor {

    @NotNull
    @Override
    protected String getPrefix(long count) {
        String prefix = count + " ";
        if (count % 3 == 0) {
            return prefix + "3-fold input: ";
        }
        return prefix;
    }
}
