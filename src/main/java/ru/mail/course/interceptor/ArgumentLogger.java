package ru.mail.course.interceptor;

import lombok.NoArgsConstructor;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mail.course.logger.Logger;

import javax.inject.Inject;

@NoArgsConstructor
public final class ArgumentLogger implements MethodInterceptor {
    @NotNull
    @Inject
    private Logger logger;

    @Nullable
    @Override
    public Object invoke(@NotNull MethodInvocation invocation) throws Throwable {
        Object[] arguments = invocation.getArguments();
        if (arguments.length > 0) {
            if (arguments[0] instanceof String) {
                logger.log(arguments[0].toString());
            }
        }
        return invocation.proceed();
    }
}
