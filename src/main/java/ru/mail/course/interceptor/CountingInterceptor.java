package ru.mail.course.interceptor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

public abstract class CountingInterceptor implements MethodInterceptor {
    @NotNull
    @Inject
    private LongAdder counter;

    @Nullable
    private Object targetObj;

    @Nullable
    @Override
    public Object invoke(@NotNull MethodInvocation invocation) throws Throwable {
        if (targetObj != null && targetObj == invocation.getThis()) {
            targetObj = null;
            return invocation.proceed();
        }
        Object arg = invocation.getArguments()[0];
        counter.increment();
        if (arg instanceof String) {
            Method method = invocation.getMethod();
            Object target = invocation.getThis();
            targetObj = target;
            method.invoke(target, getPrefix(counter.longValue()) + arg);
            return null;
        }
        return invocation.proceed();
    }

    @NotNull
    protected abstract String getPrefix(long count);
}
