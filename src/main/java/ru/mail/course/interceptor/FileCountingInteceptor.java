package ru.mail.course.interceptor;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class FileCountingInteceptor extends CountingInterceptor {
    @NotNull
    @Override
    protected String getPrefix(long count) {
        return count + " ";
    }
}
