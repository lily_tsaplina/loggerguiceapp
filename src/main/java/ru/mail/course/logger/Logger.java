package ru.mail.course.logger;

import org.jetbrains.annotations.NotNull;

public interface Logger {
    void log(@NotNull String output);
}
