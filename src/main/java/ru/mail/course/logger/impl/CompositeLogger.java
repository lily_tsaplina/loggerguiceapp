package ru.mail.course.logger.impl;

import org.jetbrains.annotations.NotNull;
import ru.mail.course.logger.Logger;

import javax.inject.Inject;

public final class CompositeLogger implements Logger {
    @NotNull
    private ConsoleLogger consoleLogger;
    @NotNull
    private FileLogger fileLogger;

    @Inject
    public CompositeLogger(@NotNull ConsoleLogger consoleLogger, @NotNull FileLogger fileLogger) {
        this.consoleLogger = consoleLogger;
        this.fileLogger = fileLogger;
    }

    @Override
    public void log(@NotNull String output) {
        consoleLogger.log(output);
        fileLogger.log(output);
    }
}
