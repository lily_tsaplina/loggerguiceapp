package ru.mail.course.logger.impl;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.annotation.CountingForConsole;
import ru.mail.course.logger.Logger;

@SuppressWarnings("FinalOrAbstractClass")
@AllArgsConstructor
public class ConsoleLogger implements Logger {
    @CountingForConsole
    @Override
    public void log(@NotNull String output) {
        System.out.println(output);
    }
}
