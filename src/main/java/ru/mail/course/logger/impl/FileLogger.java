package ru.mail.course.logger.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mail.course.FileLoggerMeta;
import ru.mail.course.annotation.CountingForFile;
import ru.mail.course.logger.Logger;

import javax.inject.Inject;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@SuppressWarnings("FinalOrAbstractClass")
@NoArgsConstructor
@AllArgsConstructor
public class FileLogger implements Logger {
    @NotNull
    @Inject
    private FileLoggerMeta fileLoggerMeta;

    @Override
    @CountingForFile
    public void log(@NotNull String output) {
        String tag = fileLoggerMeta.getTag();
        output = "<" + tag + ">"
                + output
                + "<" + tag + "/>";
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileLoggerMeta.getFileName(), true))) {
            writer.write(output);
            writer.write(System.lineSeparator());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
