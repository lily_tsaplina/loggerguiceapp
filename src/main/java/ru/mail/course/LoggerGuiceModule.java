package ru.mail.course;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.mail.course.annotation.CountingForConsole;
import ru.mail.course.annotation.CountingForFile;
import ru.mail.course.annotation.Logging;
import ru.mail.course.interceptor.ArgumentLogger;
import ru.mail.course.interceptor.FileCountingInteceptor;
import ru.mail.course.interceptor.ConsoleCountingInterceptor;
import ru.mail.course.logger.Logger;
import ru.mail.course.logger.impl.CompositeLogger;
import ru.mail.course.logger.impl.ConsoleLogger;
import ru.mail.course.logger.impl.FileLogger;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

import static com.google.inject.matcher.Matchers.annotatedWith;
import static com.google.inject.matcher.Matchers.any;

@AllArgsConstructor
public final class LoggerGuiceModule extends AbstractModule {
    @NotNull
    LoggerType loggerType;
    @NotNull
    String fileName;
    @NotNull
    String tag;

    @Override
    protected void configure() {
        bind(LongAdder.class).toInstance(new LongAdder());
        bind(FileLoggerMeta.class)
                .toInstance(new FileLoggerMeta(fileName, tag));

        switch (loggerType) {
            case CONSOLE: {
                bind(Logger.class)
                        .to(ConsoleLogger.class)
                        .in(Scopes.SINGLETON);
            }
                break;
            case FILE: {
                bind(Logger.class)
                        .to(FileLogger.class)
                        .in(Scopes.SINGLETON);
            }
                break;
            case COMPOSITE: bind(Logger.class)
                    .to(CompositeLogger.class)
                    .in(Scopes.SINGLETON);
                break;
        }

        ConsoleCountingInterceptor consoleCountingInterceptor = new ConsoleCountingInterceptor();
        requestInjection(consoleCountingInterceptor);
        bindInterceptor(
                any(),
                annotatedWith(CountingForConsole.class),
                consoleCountingInterceptor
        );

        FileCountingInteceptor fileCountingInteceptor = new FileCountingInteceptor();
        requestInjection(fileCountingInteceptor);
        bindInterceptor(
                any(),
                annotatedWith(CountingForFile.class),
                fileCountingInteceptor
        );

        ArgumentLogger argumentLogger = new ArgumentLogger();
        requestInjection(argumentLogger);
        bindInterceptor(
            any(),
            annotatedWith(Logging.class),
            argumentLogger
        );
    }
}
