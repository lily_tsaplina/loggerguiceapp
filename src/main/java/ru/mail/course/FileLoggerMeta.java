package ru.mail.course;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
@NoArgsConstructor
public final class FileLoggerMeta {
    @NotNull
    @Getter
    String fileName;

    @NotNull
    @Getter
    String tag;
}
